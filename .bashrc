# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
# local installation for example for brew
if [ -f /usr/local/etc/bash_completion ] && ! shopt -oq posix; then
    . /usr/local/etc/bash_completion
fi

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

if [ -z "$USER" ]; then
    export USER=`whoami`
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#set terminal to maximum colors
# export TERM=xterm-256color
export TERM=screen-256color
# altermative in source: http://www.linuxized.com/2010/05/switching-from-gnu-screen-to-tmux/
color_prompt=yes

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
    color_prompt=
    fi
fi

# using for __my_git_ps1
source "$HOME/bin/git-prompt.sh"
if [ "$color_prompt" = yes ]; then
    # only last dir instead of path W instead of w + git branch $(__git_ps1)
    PS1='\[\033[01;33m\]\u@\h:\[\033[00;35m\]$(__my_git_ps1)\[\033[01;34m\]\W\[\033[01;34m\]\$\[\033[00m\] '
else
    # only last dir instead of path W instead of w
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\W$(__my_git_ps1)\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    # only last dir instead of path W instead of w
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\h: \W\a\]$PS1"
    ;;
*)
    ;;
esac

alias ll='ls -alF'
alias l='ls -BlF'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

function silent () { "$@" >/dev/null 2>&1 & }
function pathname() { command echo $(printf '%q'/'%q' "`pwd -P`" "$1") ; }
alias t='tmux attach'
alias tm='tmux'
alias c=cd
alias 'hglog'='hg log --color=always| less -r -X'
alias 'hgglog'='hg glog --color=always| less -r -X'
alias 'svnlog'='svn log | less -X'
alias vi='nvim'
alias vimdiff='nvim -d'
alias view='nvim -R'

alias hh=history  # Use history and !number for obtaining the command from history
# Store all history with times and directories
function store_history () {
    history 1 | gawk '($2 !~ "^[mr]?cd[0-9a-z]?$") {$1="_T="strftime("%Y%m%d_%H:%M:%S_") PROCINFO["ppid"] "_PWD="  ENVIRON["PWD"] "\t"; $2=gensub("^_T=[-_0-9:]*[ \t]* *", "", 1, $2); $2=gensub("^_P=[^ \t]* *", "", 1, $2); print;}' >> ~/.history-all-$USER
}
# export PROMPT_COMMAND="store_history; $PROMPT_COMMAND"
export PROMPT_COMMAND="store_history"

# Grep history
function h (){
  if [[ -z "$DIR" ]] ; then
    DIR="$PWD"
  fi
  if [[ -z "$H_LINES" ]] ; then
    H_LINES=30
  fi
  command grep -a "_PWD=$DIR"$'\t'".*$@" ~/.history-all-$USER | tail -n $H_LINES
}

# TODO ah less with complete history
function ah (){
  if [[ -z "$AH_LINES" ]] ; then
    AH_LINES=30
  fi
  if [ "$#" -eq 0 ]; then
      tail -n $AH_LINES ~/.history-all-$USER
  else
      command grep -a "$@" ~/.history-all-$USER | sed 's:\t:\n\t:' | tail -n $AH_LINES
  fi
}


#}}}

### Exports
#{{{
export PATH=$HOME/bin:$HOME/.local/bin:$PATH
export PYTHONPATH=$HOME/bin:"$PYTHONPATH"

export LANG=en_US.UTF-8
#export LANG=cs_CZ.UTF-8
export LC_ALL="$LANG"
export LANGUAGE="$LANG"

export EDITOR=vim # nice for git default editor
#}}}

# vi editing mode for bash
# examples on http://www.catonmat.net/blog/bash-vi-editing-mode-cheat-sheet/
set -o vi

### autojump ###
[[ -s /usr/share/autojump/autojump.sh ]] && . /usr/share/autojump/autojump.sh
[[ -s ~/.autojump/etc/profile.d/autojump.sh ]] && . ~/.autojump/etc/profile.d/autojump.sh

# stop terminal freezing
stty ixany

# profile performance
alias wotgobblemem='ps -o time,ppid,pid,nice,pcpu,pmem,user,comm -A | sort -n -k 6 | tail -15'
alias ds='du -ks *|sort -n'

# loading optional settings
for f in bashrc_local bash_sshali bashrc_osx bashrc_lxde ; do
    if [ -f ~/.$f ] ; then source ~/.$f; fi
done

alias ipc='ipython console'

alias fuck='eval $(thefuck $(fc -ln -1)); history -r'
alias f='fuck'

alias shellgit='git --git-dir=$HOME/.shellgit.git/ --work-tree=$HOME'

function  preview-image () {
  args="--background=light --colors"
  img="$1"
  if [ ${img: -4} == ".jpg" ] || [ ${img: -4} == ".jpeg" ] ; then
    jp2a $args $img
  else
    convert -quality 100 "$img" jpg:- | jp2a $args -
  fi
}

function view-theano-config () {
  python -c 'import theano; print theano.config' | less
}


# Compile a LaTeX source into PDF using PDFLaTeX and BibTeX
function mkpdf (){
    file=`basename $1 .tex`
    pdflatex $file
    bibtex $file || ( echo "Bibtex failed" && exit 1 )
    lim=4
    while [ $lim -ge 0 ] \
            && grep 'Rerun to get cross-references right.\|Citation.*undefined' $file.log >/dev/null 2>/dev/null; do \
        pdflatex $file
	lim=$(($lim - 1))
    done
    pdflatex $file
}

declare -a COLORS=('1;30' '1;31' '1;32' '0;33' '1;33' '1;34' '0;35' '1;35' '0;36' '1;36' '1;37')

# TODO add also the linking and compilation variables RUN_LIB_PATH INCLUDE_PATH etc
export PATH=/usr/local/bin:$PATH
# export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
