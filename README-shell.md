# shellgit is repository for command line settings, especially bash #

## Installation ##
See bin/setting_env.sh
You can clone it (readonly) by:

```bash
	git clone https://oplatek@bitbucket.org/oplatek/shellgit.git ~/shellgit.tmp
    cp -r ~/shellgit.tmp/.git ~/.shellgit.git
	alias shellgit='git --git-dir=$HOME/.shellgit.git/ --work-tree=$HOME'
	cd $HOME; shellgit checkout .  # DANGEROUS ;-)
```


To browse source code go to https://bitbucket.org/oplatek/shellgit/src/

### Stores ###

* bashrc
* commandline scripts
* should store completition files


### TODO
- How to install github distributed code?
    - e.g. https://github.com/dkogan/feedgnuplot
