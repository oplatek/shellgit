###############################
#  Examples commands for gdb  #
###############################

# Open TUI mode in viewing src code
# layout src  

# we want to debug this program 
# file ./online-python-gmm-decode-faster-test 

# add directory to search path for source codes
# directory ../feat 

# set breakpoint
# b online-python-gmm-decode-faster-test.cc:80 

# pass arguments to program
# set args --rt-min=0.5 --rt-max=0.7

# execute shell command ls
# shell ls  

#############################
#  EXAMPLES how to run gdb  #
#############################
# gdb -q -iex "set auto-load safe-path ." .gdbinit  
# gdb -q -x .gdbinit

#######################
#  USEFULL shortcuts  #
#######################
# Ctrl+x Ctrl+a ... switches of and of tui from gdb prompt
# In TUI mode: Ctrl+n  resp. Ctrl+p ... next resp. previous line in history
# Ctrl + L refresh screen

###########
#  LINKS  #
###########
# Gdb manual
# http://ftp.gnu.org/old-gnu/Manuals/gdb-5.1.1/html_chapter/gdb_19.html
# Martin Jiricka slides in czech: 
# http://www.ms.mff.cuni.cz/~jirim7am/data/gdb/gdb.pdf (or in my Calibre library)


#############################
#  LOADING prepared tweaks  #
#############################
# For pretty printing containers. E.g pvector. For more "help pstl".
source ~/.gdb-stl-views.gdb

# From usr/share/doc/python2.7/gdbinit.gz 
# Use it with instruction from http://wiki.python.org/moin/DebuggingWithGdb
# and https://fedoraproject.org/wiki/Features/EasierPythonDebugging
# TODO before using it RUN sudo apt-get install gdb python2.7-dbg !
source ~/.gdb-python.gdb

##############
#  Settings  #
##############

set history save on 
# if not spefified otherwise save the history to HOME 
set history filename ~/.gdb_history 
# waits if the sharedlib is not loaded
set breakpoint pending on    

#  Make C++ looks better 
set print pretty on
set print object on
set print static-members on
set print vtbl on
set print demangle on
set print asm-demangle on
set demangle-style gnu-v3
set print sevenbit-strings off

###############
#  Utilities  #
###############

define colors
    # may break some commands - with TUI
    source ~/.gdb-colors.gdb 
end
document colors
Color some of the commands bt, up, down.
It uses definitions stored at ~/.gdb-colors.gdb
end

define uncolors
source ~/.gdb-uncolors.gdb 
document colors
It redefines -> disables definitions from ~/.gdb-colors.gdb
end

define run_and_exit
    catch throw
    python gdb.events.exited.connect(lambda x : gdb.execute("quit"))
    run
end
document run_and_exit
Runs the program and exits if no error occur.
end
