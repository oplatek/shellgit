

def load_ipython_extension(ip):
    try:
        import memory_profiler
        ip.define_magic('memit', memory_profiler.magic_memit)
        ip.define_magic('mprun', memory_profiler.magic_mprun)
    except ImportError:
        print('memory_profiler not installed')
