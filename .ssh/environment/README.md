Usefull for ssh agent on remote servers
=======================================

Installation
-------------
Copy and adjust the following to your bashrc.

```
containsElement () {
  local e
  for e in ${@:2}; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

alias ssh='ssh -AX'
allowed_ssh_agents=('server')  # login or work machines
agent_file_prefix="$HOME/.ssh/environment"
hostname=`hostname -s`
agent_file="${agent_file_prefix}/${hostname}"

mkdir -p $agent_file_prefix

#### DEPRECATED by using ssh -A from my notebook ######
### See http://rabexc.org/posts/using-ssh-agent ###
### Still not working in mosh. See https://github.com/mobile-shell/mosh/issues/120 . Let me know if they fixed it! ###
function start_agent () {
  >&2 echo "Initialising new SSH agent for $hostname"
  ssh-agent | sed 's/^echo/#echo/' > "$agent_file"
  chmod 600 "$agent_file"
  . "$agent_file" > /dev/null
  # ssh-add -l >/dev/null || { alias ssh='ssh-add -l >/dev/null || ssh-add && alias ssh="ssh -XYC"; ssh -XYC'; } FIXME why one would do it this way? 
  ssh-add -l >/dev/null || ssh-add  > /dev/null || >&2 echo "Adding ssh-key to ss-agent failed. Ssh-agen will not probably work."
  use_ssh_agent
}

function use_ssh_agent() {
  alias ssh="ssh -A" # Force X-forwarding (+alias adding the key to agent)
  ssh_pid=$(cat $agent_file 2> /dev/null | grep 'AGENT_PID' | sed 's/.*=\([0-9]*\);.*/\1/')
  >&2 echo "Using SSH agent(pid: $ssh_pid) on $hostname"
}

# Source SSH settings, if applicable
if ( tty -s) ; then
  if containsElement "$hostname" "${allowed_ssh_agents[@]}" ; then
    if [ ! -f "${agent_file}" ] ; then
      >&2 echo "No settings for ssh agent found. Creating new."
      start_agent
     else
       . "${agent_file}" > /dev/null
       pid_name=$(ps -ef | grep "$SSH_AGENT_PID" | grep ssh-agent | rev | cut -d ' ' -f 1 | rev 2> /dev/null)
       if [[ "$pid_name" != 'ssh-agent' ]] ; then
         >&2 echo "Ssh agent with PID $SSH_AGENT_PID is not running anymore.  (Found process '$pid_name' instead). Restarting."
         start_agent
       else
         >&2 echo "Found running ssh-agent $agent_pid"
         use_ssh_agent
       fi
     fi
  else
    >&2 echo "SSH agent on $hostname is not allowed."
  fi
fi

sshcwd () {
  # save current running history so that it is available
  # immediatelly on the remote machine
  history -a;
  # setup the working directory by setting WD
  ssh -X -Y -t $@ "SSHAUTOWD='$PWD' /bin/bash --login -i";
}
# use WD to setup the working directory
if [ -n "$SSHAUTOWD" ]; then
  echo "Used working directory from previous computer"
  cd $SSHAUTOWD;
fi
# don't export the variable further, it would confuse qsub etc...
export -n SSHAUTOWD=""


# On TTYs only:
if ( tty -s ); then
    # Greeting
    echo -e 'Hi, this is\033[0;'${COLOR}'m' `hostname` '\033[0m' 1>&2
fi
```
