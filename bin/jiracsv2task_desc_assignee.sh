#!/bin/sh
jira_csv="$1"
cut -d',' -f3,5,7 "$jira_csv" | \
    sed 's/,/ /g' | \
    sed 's/ondrej.platek/(OP)/' | \
    sed 's/adrian.lachata/(AL)/' | \
    sed 's/vojta.hudecek/(VH)/'

