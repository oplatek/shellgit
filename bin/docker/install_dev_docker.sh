#!/bin/bash
set -e
apt-get update && apt-get install -y \
  atool \
  autojump \
  build-essential \
  cgdb \
  cmake \
  gawk \
  gfortran \
  git \
  graphviz \
  libatlas-base-dev \
  libffi-dev \
  libssl-dev \
  mosh \
  openssl \
  python-dev \
  python-matplotlib \
  python-setuptools \
  sox \
  subversion \
  swig \
  tmux \
  vim \
  wget

easy_install pip

mkdir -p /home/oplatek
export HOME=/home/oplatek
cd $HOME

git clone https://oplatek@bitbucket.org/oplatek/shellgit.git
cp shellgit/.bashrc $HOME/.bashrc
cp -r shellgit/.git $HOME/.shellgit.git
git --git-dir=$HOME/.shellgit.git/ --work-tree=$HOME checkout .

pip install --upgrade -r bin/oplatek_requirements_pip.txt

git clone https://oplatek@bitbucket.org/oplatek/dotfiles-vim.git .vim
echo todo install plugins
# vim '+PluginInstall!' '+qall'
# $HOME/.vim/bundle/YouCompleteMe/install.sh --clang-completer

# install project requirements
wget https://raw.githubusercontent.com/UFAL-DSG/pykaldi/master/pykaldi/pykaldi-requirements.txt -O /tmp/pykaldi-requirements.txt && pip install -r /tmp/pykaldi-requirements.txt
wget https://raw.githubusercontent.com/UFAL-DSG/alex/master/alex-requirements.txt -O /tmp/alex-requirements.txt && pip install -r /tmp/alex-requirements.txt
wget https://raw.githubusercontent.com/oplatek/cleverobot/master/app-requirements.txt -O /tmp/cb-app.txt && pip install -r /tmp/cb-app.txt
wget https://github.com/oplatek/cleverobot/blob/master/bot-requirements.txt -O /tmp/cb-bot.txt && pip install -r /tmp/cb-bot.txt


