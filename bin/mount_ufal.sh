#!/bin/bash
echo "Mount: mount-discs.sh"
echo "Unmount: mount-discs.sh u"

# login
NAME=oplatek

DIR_HOME=$HOME/ufal-home
DIR_WORK=$HOME/ufal-cluster

if [ x$1 = xu ]; then
	fusermount -u `pwd`/$DIR_HOME;
	fusermount -u `pwd`/$DIR_WORK;
else
	echo
	echo "Mounting directories as user $NAME";

	echo "Working directory";
	mkdir -p $DIR_WORK
	sshfs $NAME@shrek.ms.mff.cuni.cz:/net/work/people/$NAME ./$DIR_WORK

	echo "Home directory";
	mkdir -p $DIR_HOME
	sshfs $NAME@shrek.ms.mff.cuni.cz: ./$DIR_HOME
fi;
