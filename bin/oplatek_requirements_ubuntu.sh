#!/bin/bash

echo ; echo "Installing packages as needed for Lubuntu 14.04" ; echo
sudo apt-get update || exit 1
sudo apt-get install -y jq vim-gtk git subversion atool mosh cmake cgdb gimp light-locker inkscape chromium-browser tmux openssl python-setuptools python-dev python-matplotlib libssl-dev gdebi  sox vlc audacity gfortran libatlas-base-dev portaudio19-dev swig flac speex libsox-dev mplayer libsqlite3-dev python-wxgtk2.8 libmad0-dev libsox-dev  ubuntu-restricted-extras libavcodec-extra icedtea-7-plugin openjdk-7-jre openjdk-7-jdk texlive-full texlive-lang-czechslovak doxygen pandoc autojump python-graph-tool cmus nodejs npm || exit 1


npm install -g json2csv

# TODO install https://github.com/jehiah/json2csv
# TODO http://csvkit.readthedocs.io/en/0.9.1/
# TODO learn and install hdf5 https://www.hdfgroup.org/HDF5/doc/RM/Tools.html

#problematic
# gstreamer0.10-ffmpeg
echo; echo "install flash plugin" ; echo
sudo apt-get install pepperflashplugin-nonfree && sudo update-pepperflashplugin-nonfree --install 


# echo ; echo " Specific K53s - nvidia card " ; echo
# sudo apt-get install bumblebee bumblebee-nvidia primus linux-headers-generic  || exit 1


echo ; echo "install Python modules"; echo
sudo easy_install pip || exit 1
sudo pip install --upgrade -r ./oplatek_requirements_pip.txt || exit 1

echo ; echo "dependencies for pwsafe" ; echo
sudo apt-get install libncurses5 libncurses5-dev libxmu-dev libreadline-dev || exit 1


echo ; echo "TODOS"; echo
echo "install pwsafe git clone https://github.com/oplatek/pwsafe; cd pwsafe ; ./configure && make && sudo make install"
echo "install dropbox; setup dropbox"

# Extra repositories
echo "TODO add graphviz repo"
exit 1
sudo apt-get install graphviz
