#!/bin/bash

. ~/.bash_sshali
. ~/bin/parse_options.sh


if [ $# != 1 ]; then
  echo "usage: $0 <machine-name>"
  exit 1
fi

machine_name=$1
docker-machine create -d generic \
  --generic-ip-address $metacentrum_public_ip \
  --generic-ssh-key ~/.ssh/id_rsa \
  $machine_name
