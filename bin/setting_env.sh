#!/bin/bash

set -e

if [ "$USER" == root ] ; then
    SUDO=""
else
    SUDO=sudo
fi

echo ; echo "Installing tools and set settings for ubuntu" ; echo
$SUDO apt-get update
$SUDO apt-get install -y curl gfortran vim subversion tmux git clang cmake python-dev python-pip autojump gawk libatlas-base-dev


tmp=$(mktemp -d)
git clone https://oplatek@bitbucket.org/oplatek/shellgit.git --separate-git-dir=$HOME/.shellgit.git $tmp
pushd $HOME
    alias shellgit="git --git-dir=$HOME/.shellgit.git/ --work-tree=$HOME"
    git --git-dir=$HOME/.shellgit.git/ --work-tree=$HOME checkout .
popd

$SUDO pip install -r $HOME/bin/oplatek_requirements_pip.txt

# git clone https://oplatek@bitbucket.org/oplatek/dotfiles-vim.git $HOME/.vim
# vim '+BundleInstall!' '+qall'
# pushd $HOME/.vim/bundle/YouCompleteMe
# ./install.sh --clang-completer
# popd

easy_install3
pip3

pip3 install --upgrade neovim
flake8
pip3 install matlotlib
sudo apt-get install  python3-tk   # for matplotlib

# https://askubuntu.com/questions/22207/quickly-place-a-window-to-another-screen-using-only-the-keyboard
# add /usr/local/lib to ldconfig: see https://unix.stackexchange.com/questions/67781/use-shared-libraries-in-usr-local-lib
todo nvim
sudo apt-get install autogen autoconf libtool htop
